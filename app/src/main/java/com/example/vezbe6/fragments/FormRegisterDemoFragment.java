package com.example.vezbe6.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.example.vezbe6.R;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FormRegisterDemoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FormRegisterDemoFragment extends Fragment {

    public static FormRegisterDemoFragment newInstance() {
        return new FormRegisterDemoFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form_register_demo, container, false);

        final TextInputEditText email = view.findViewById(R.id.email);
        Button submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            final String regExp = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$";
            @Override
            public void onClick(View view) {
                TextInputLayout emailLayout = (TextInputLayout) email.getParent().getParent();
                if(email.getText().toString().trim().isEmpty())
                    emailLayout.setError(getString(R.string.required));
                else if(!email.getText().toString().matches(regExp))
                    emailLayout.setError(getString(R.string.email_err));
                else {
                    emailLayout.setError(null);
                    Toast.makeText(getActivity(), "Form is valid!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }
}